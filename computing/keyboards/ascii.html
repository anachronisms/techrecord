<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="an_achronism">
        <link rel="canonical" href="https://www.techrecord.info/computing/keyboards/ascii.html" />
        <link rel="stylesheet" href="../../style.min.css">
        <link rel="icon" type="image/x-icon" href="../../favicon.ico">
        <title>ASCII</title>
    </head>
    <body>
        <nav>
            <ul>
                <li><a href="../../">Home</a></li>
                <li><a href="./">Keyboards</a></li>
                <li><a href="../../about.html">About</a></li>
            </ul>
        </nav>
        <main>
            <h1>ASCII: The American Standard for Information Interchange</h1>
            <p>
                In May of 1961, the American Standards Association's X3.2
                subcommittee met to discuss standardising a new system for
                encoding characters in telecommunications and computing devices.
                The resulting 7-bit standard, first published in 1963, allowed a
                total of no more than 128 code points to be assigned in an
                organised manner that would allow devices to communicate with
                one another unambiguously. The initial published standard was
                revised several times, however, and some of the changes made in
                those revisions are as important as the original standard when
                looking at ASCII's impact on computing in the present day. This
                page aims to present some key information about the initial
                standard and its early revisions in order to contextualise them
                in a way that will hopefully be informative and interesting,
                even if you know nothing whatsoever about the topic beforehand.
            </p>
            <p>
                Each of the characters of the ASCII are encoded in a byte
                containing 7 bits of data, each bit being represented by a
                binary value of either 0 or 1. (Today, it is generally more
                common to think of a byte as consisting of multiples of 8 bits.)
                I don't want to get overly bogged down in how computers encode
                data in general here, but I will briefly acknowledge the
                concept of bit indexing only to explain that a byte is written,
                in effect, from right to left. In other words, the "first" bit,
                which is known as the <em>least significant bit (LSb)</em>, is
                the one written at the end of the number. Unsurprisingly, the
                bit written first (the one on the far left) is known as the
                <em>most significant bit (MSb)</em>. For example, in the ASCII
                character 1001100 (which happens to be the letter L), the bits
                are numbered as follows:
            </p>
            <table>
                <tr>
                    <th>bit 7<br />(MSb)</th>
                    <th>bit 6</th>
                    <th>bit 5</th>
                    <th>bit 4</th>
                    <th>bit 3</th>
                    <th>bit 2</th>
                    <th>bit 1<br />(LSb)</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                </tr>
            </table>
            <p>
                <em>
                    (Note: Yes, if you're wondering, the ASCII indexes bits from
                    1, not 0. If you weren't wondering, don't worry about it.)
                </em>
            </p>
            <p>
                The complete array of ASCII code points (each code point
                representing one character) is defined in a table which helps to
                visualise how the characters are organised. Remember the bit
                order when reading the tables, i.e. bits 5 to 7 are the ones on
                the left, and bits 1 to 6 are the ones on the right.
            </p>
            <p>
                Not every character is a glyph: several are "control characters"
                which have special functions that I'll explain later.
            </p>
            <br />
            <h2>American Standard Code for Information Interchange (ASA
                X3.4-1963)</h2>
            <p>
                The original publication of the ASCII defined the following 100
                code points, leaving 28 undefined for future standardisation:
            </p>
            <table class="codepoints">
                <tr>
                    <th>bits 5 to 7 &rarr;<br />bits 1 to 4 &darr;</th>
                    <th>000</th>
                    <th>001</th>
                    <th>010</th>
                    <th>011</th>
                    <th>100</th>
                    <th>101</th>
                    <th>110</th>
                    <th>111</th>
                </tr>
                <tr>
                    <th>0000</th>
                    <td>NULL</td>
                    <td>DC<sub>0</sub></td>
                    <td>&#x180;</td>
                    <td>0</td>
                    <td>@<sup>*</sup></td>
                    <td>P</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>0001</th>
                    <td>SOM</td>
                    <td>DC<sub>1</sub></td>
                    <td>!</td>
                    <td>1</td>
                    <td>A</td>
                    <td>Q</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>0010</th>
                    <td>EOA</td>
                    <td>DC<sub>2</sub></td>
                    <td>"</td>
                    <td>2</td>
                    <td>B</td>
                    <td>R</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>0011</th>
                    <td>EOM</td>
                    <td>DC<sub>3</sub></td>
                    <td>#</td>
                    <td>3</td>
                    <td>C</td>
                    <td>S</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>0100</th>
                    <td>EOT</td>
                    <td>DC<sub>4</sub><br /><small>(STOP)</small></td>
                    <td>$</td>
                    <td>4</td>
                    <td>D</td>
                    <td>T</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>0101</th>
                    <td>WRU</td>
                    <td>ERR</td>
                    <td>%</td>
                    <td>5</td>
                    <td>E</td>
                    <td>U</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>0110</th>
                    <td>RU</td>
                    <td>SYNC</td>
                    <td>&</td>
                    <td>6</td>
                    <td>F</td>
                    <td>V</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>0111</th>
                    <td>BELL</td>
                    <td>LEM</td>
                    <td>'</td>
                    <td>7</td>
                    <td>G</td>
                    <td>W</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>1000</th>
                    <td>FE<sub>0</sub></td>
                    <td>S<sub>0</sub></td>
                    <td>(</td>
                    <td>8</td>
                    <td>H</td>
                    <td>X</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>1001</th>
                    <td>HT/SK</td>
                    <td>S<sub>1</sub></td>
                    <td>)</td>
                    <td>9</td>
                    <td>I</td>
                    <td>Y</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>1010</th>
                    <td>LF</td>
                    <td>S<sub>2</sub></td>
                    <td>*</td>
                    <td>:<sup>*</sup></td>
                    <td>J</td>
                    <td>Z</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>1011</th>
                    <td>V<sub>TAB</sub></td>
                    <td>S<sub>3</sub></td>
                    <td>+</td>
                    <td>;<sup>*</sup></td>
                    <td>K</td>
                    <td>[<sup>*</sup></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>1100</th>
                    <td>FF</td>
                    <td>S<sub>4</sub></td>
                    <td>,</td>
                    <td><</td>
                    <td>L</td>
                    <td>\<sup>*</sup></td>
                    <td></td>
                    <td>ACK</td>
                </tr>
                <tr>
                    <th>1101</th>
                    <td>CR</td>
                    <td>S<sub>5</sub></td>
                    <td>-</td>
                    <td>=</td>
                    <td>M</td>
                    <td>]<sup>*</sup></td>
                    <td></td>
                    <td>&#x2460;</td>
                </tr>
                <tr>
                    <th>1110</th>
                    <td>SO</td>
                    <td>S<sub>6</sub></td>
                    <td>.</td>
                    <td>></td>
                    <td>N</td>
                    <td>&uarr;<sup>*</sup></td>
                    <td></td>
                    <td>ESC</td>
                </tr>
                <tr>
                    <th>1111</th>
                    <td>SI</td>
                    <td>S<sub>7</sub></td>
                    <td>/</td>
                    <td>?</td>
                    <td>O</td>
                    <td>&larr;<sup>*</sup></td>
                    <td></td>
                    <td>DEL</td>
                </tr>
            </table>
            <p>
                All blank positions in this table represent codes that are
                theoretically available for use but not standardised.
            </p>
            <p>
                <sup>*</sup> The document also suggests some non-standard
                functions that could hypothetically be assigned to some code
                points:<br />
                <div class="quote">
                    <em>
                        "The five graphics immediately following the letter Z
                        can be replaced by the additional letters required for
                        complete expression of certain European alphabets.
                        Further, the single position preceding the letter A can
                        be used for those alphabets requiring 32 characters. In
                        most cases, only three additional letters will be
                        required.<br /><br />
                        For those applications requiring use of the sterling
                        monetary system or duodecimal arithmetic, the digits 10
                        and 11 can replace the two graphics immediately
                        following the digit 9."
                    </em>
                </div>
            </p>
            <h3>Control characters</h3>
            <p>
                The ASCII includes a number of “control characters”, which it
                organises (somewhat loosely) into four categories:
            </p>
            <ol>
                <li>Transmission controls</li>
                <li>Format effectors</li>
                <li>Device controls</li>
                <li>Information separators</li>
            </ol>
            <p>
                However, it doesn't clearly designate which ones are intended to
                belong to which groups, other than explaining that they are
                largely chunked together as they appear in the code block.
                Context more or less fills in the gaps, though.
            </p>
            <table>
                <tr>
                    <th>Abbr.</th>
                    <th>Full name</th>
                    <th>Type (inferred)</th>
                </tr>
                <tr>
                    <td>SOM</td>
                    <td>Start of message</td>
                    <td>Transmission control</td>
                </tr>
                <tr>
                    <td>EOA</td>
                    <td>End of address</td>
                    <td>Transmission control</td>
                </tr>
                <tr>
                    <td>EOM</td>
                    <td>End of message</td>
                    <td>Transmission control</td>
                </tr>
                <tr>
                    <td>WRU</td>
                    <td>"Who are you?"</td>
                    <td>Transmission control</td>
                </tr>
                <tr>
                    <td>RU</td>
                    <td>"Are you...?"</td>
                    <td>Transmission control</td>
                </tr>
                <tr>
                    <td>FE<sub>0</sub></td>
                    <td>Format effector</td>
                    <td>Format effector</td>
                </tr>
                <tr>
                    <td>HT/SK</td>
                    <td>Horizontal tabulation / skip (punched card)</td>
                    <td>Format effector</td>
                </tr>
                <tr>
                    <td>LF</td>
                    <td>Line feed</td>
                    <td>Format effector</td>
                </tr>
                <tr>
                    <td>V<sub>TAB</sub></td>
                    <td>Vertical tabulation</td>
                    <td>Format effector</td>
                </tr>
                <tr>
                    <td>FF</td>
                    <td>Form feed</td>
                    <td>Format effector</td>
                </tr>
                <tr>
                    <td>CR</td>
                    <td>Carriage return</td>
                    <td>Format effector</td>
                </tr>
                <tr>
                    <td>SO</td>
                    <td>Shift out</td>
                    <td>Format effector</td>
                </tr>
                <tr>
                    <td>SI</td>
                    <td>Shift in</td>
                    <td>Format effector</td>
                </tr>
                <tr>
                    <td>DC<sub>0&ndash;4</sub></td>
                    <td>Device control 0&ndash;4</td>
                    <td>Device controls</td>
                </tr>
                <tr>
                    <td>ERR</td>
                    <td>Error</td>
                    <td>Transmission control</td>
                </tr>
                <tr>
                    <td>SYNC</td>
                    <td>Synchronous idle</td>
                    <td>Transmission control</td>
                </tr>
                <tr>
                    <td>LEM</td>
                    <td>Logical end of media</td>
                    <td>Transmission control</td>
                </tr>
                <tr>
                    <td>S<sub>0&ndash;7</sub></td>
                    <td>Separator 0&ndash;7</td>
                    <td>Information separators</td>
                </tr>
                <tr>
                    <td>&#x180;</td>
                    <td>Word separator / space (generally non-printing)</td>
                    <td>Format effector</td>
                </tr>
                <tr>
                    <td>ACK</td>
                    <td>Acknowledge</td>
                    <td>Transmission control</td>
                </tr>
                <tr>
                    <td>&#x2460;</td>
                    <td>described as an unassigned control character</td>
                    <td>N/A</td>
                </tr>
                <tr>
                    <td>ESC</td>
                    <td>Escape</td>
                    <td>Format effector</td>
                </tr>
                <tr>
                    <td>DEL</td>
                    <td>Delete</td>
                    <td>
                        not strictly a control character at all; rather,
                        represents all positions of a row on a punched card or
                        perforated tape being punched out: 1111111
                    </td>
                </tr>
            </table>
            <h2>Coming soon: ASA X3.4-1965 and beyond</h2>
        </main>
        <footer>
            &copy; 2023 <a href="../../about.html"
                        target="_blank"
                        rel="noopener noreferrer">an_achronism</a>
        </footer>
    </body>
</html>